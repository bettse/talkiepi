package main

import (
	"fmt"
	"io/ioutil"
	"os"
"strconv"
)

const (
	redControl   string = "/sys/class/leds/ktd202x:led1/brightness"
	greenControl string = "/sys/class/leds/ktd202x:led2/brightness"
	blueControl  string = "/sys/class/leds/ktd202x:led3/brightness"
)

type RGB struct {
	Red   byte
	Green byte
	Blue  byte
}

var (
	Red RGB = RGB{255, 0, 0}
	Green RGB = RGB{0, 255, 0}
	Blue RGB = RGB{0, 0, 255}
	White RGB = RGB{255, 255, 255}
	Off RGB = RGB{0, 0, 0}
)

func (b *Talkiepi) initLeds() {
	if _, err := os.Stat(redControl); os.IsNotExist(err) {
		return
	}
	if _, err := os.Stat(greenControl); os.IsNotExist(err) {
		return
	}
	if _, err := os.Stat(blueControl); os.IsNotExist(err) {
		return
	}

	b.ledsEnabled = true
}


func (b *Talkiepi) EnableLeds() {
	b.ledsEnabled = true
	b.setColor(b.idleColor)
}

func (b *Talkiepi) DisableLeds() {
	b.setColor(Off)
	b.ledsEnabled = false
}


func (b *Talkiepi) setColor(color RGB) {
	if b.ledsEnabled == false {
		return
	}

	err := ioutil.WriteFile(redControl, byteToBytes(color.Red), 0644)
	if err != nil {
		fmt.Printf("Error setting color %v\n", err)
	}

	err = ioutil.WriteFile(greenControl, byteToBytes(color.Green), 0644)
	if err != nil {
		fmt.Printf("Error setting color %v\n", err)
	}

	err = ioutil.WriteFile(blueControl, byteToBytes(color.Blue), 0644)
	if err != nil {
		fmt.Printf("Error setting color %v\n", err)
	}

}

func (b *Talkiepi) setIdleColor(color RGB) {
	b.idleColor = color
	b.setColor(color)
}

func (b *Talkiepi) setIdle() {
	b.setColor(b.idleColor)
}


func byteToBytes(b byte) []byte {
	i := int(b)
	s := strconv.Itoa(i)
	bs := []byte(s)
	return bs
}
