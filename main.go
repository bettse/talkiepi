package main

import (
	"os"
	"os/signal"
	"syscall"
	"crypto/rand"
	"crypto/tls"
	"flag"
	"fmt"

	"gopkg.in/robfig/cron.v2"
	"github.com/bettse/gumble/gumble"
	_ "github.com/bettse/gumble/opus"
	"github.com/brian-armstrong/gpio"
)

func main() {
	// Command line flags
	server := flag.String("server", "talkiepi.projectable.me:64738", "the server to connect to")
	username := flag.String("username", "", "the username of the client")
	password := flag.String("password", "", "the password of the server")
	insecure := flag.Bool("insecure", true, "skip server certificate verification")
	certificate := flag.String("certificate", "", "PEM encoded certificate and private key")
	channel := flag.String("channel", "talkiepi", "mumble channel to join by default")

	flag.Parse()

	// Initialize
	b := Talkiepi{
		Config:      gumble.NewConfig(),
		Address:     *server,
		ChannelName: *channel,
	}

	// if no username specified, lets just autogen a random one
	if len(*username) == 0 {
		buf := make([]byte, 6)
		_, err := rand.Read(buf)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error reading random  bytes %s\n", err)
			os.Exit(1)
		}

		buf[0] |= 2
		b.Config.Username = fmt.Sprintf("talkiepi-%02x%02x%02x%02x%02x%02x", buf[0], buf[1], buf[2], buf[3], buf[4], buf[5])
	} else {
		b.Config.Username = *username
	}

	b.Config.Password = *password

	if *insecure {
		b.TLSConfig.InsecureSkipVerify = true
	}
	if *certificate != "" {
		cert, err := tls.LoadX509KeyPair(*certificate, *certificate)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Error loading cert %s\n", err)
			os.Exit(1)
		}
		b.TLSConfig.Certificates = append(b.TLSConfig.Certificates, cert)
	}

	b.cron = cron.New()
	b.cron.AddFunc("0 22 * * *", func() { b.DisableLeds() })
	b.cron.AddFunc("30 7 * * *", func() { b.EnableLeds() })

	b.watcher = gpio.NewWatcher()
	b.watcher.AddPin(ButtonPin)
	defer b.watcher.Close()

	go func() {
		// fmt.Printf("Start go routine for watcher\n")
		b.ButtonState = 1 // Active low, so it starts high, assuming the button isn't held during startup

		for {
			_, currentState := b.watcher.Watch()
			// fmt.Printf("read %d from gpio %d\n", currentState, pin)
			if b.Stream != nil {
				if currentState != b.ButtonState {
					b.ButtonState = currentState
					if b.ButtonState == 1 {
						fmt.Printf("Button is released\n")
						b.TransmitStop()
					} else {
						fmt.Printf("Button is pressed\n")
						b.TransmitStart()
					}
				}
			}

		}
	}()

	b.Init()

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	exitStatus := 0

	<-sigs
	b.CleanUp()

	os.Exit(exitStatus)
}
