package main

import (
	"crypto/tls"

	"gopkg.in/robfig/cron.v2"
	"github.com/brian-armstrong/gpio"
	"github.com/bettse/gumble/gumble"
	"github.com/bettse/gumble/gumbleopenal"
)

// Raspberry Pi GPIO pin assignments (CPU pin definitions)
const (
	ButtonPin          uint = 23
)

type Talkiepi struct {
	Config *gumble.Config
	Client *gumble.Client

	Address   string
	TLSConfig tls.Config

	ConnectAttempts uint

	Stream *gumbleopenal.Stream

	ChannelName    string
	IsConnected    bool
	IsTransmitting bool

	ledsEnabled bool
	idleColor RGB
	watcher         *gpio.Watcher
	ButtonState     uint
	cron *cron.Cron
}
